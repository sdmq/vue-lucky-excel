import LuckyExcel from './Index.vue'

export { LuckyExcel }

const VueLuckyExcel = {
  install(App) {
    App.component('vue-lucky-excel', LuckyExcel)
  }
}

export default VueLuckyExcel

